package behaviour.Strategy;

import behaviour.Strategy.strategy.BatteryVehicleStrategy;
import behaviour.Strategy.strategy.FuelVehicleStrategy;

public class Driver {

    public static void main(String[] args) {

        Vehicle fuelVehicle=new MiniVehicle(new FuelVehicleStrategy());
        fuelVehicle.drive();

        Vehicle batteryVehicle=new MiniVehicle(new BatteryVehicleStrategy());
        batteryVehicle.drive();

    }
}
