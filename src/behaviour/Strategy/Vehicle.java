package behaviour.Strategy;

import behaviour.Strategy.strategy.DriveStrategy;

public class Vehicle {

    public Vehicle(DriveStrategy driveStrategy) {
        this.driveStrategy = driveStrategy;
    }

    private final DriveStrategy driveStrategy;

    public void drive(){
        driveStrategy.drive();
    }

}
