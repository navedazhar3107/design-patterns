package behaviour.Strategy;

import behaviour.Strategy.strategy.DriveStrategy;

public class ScooterVehicle extends Vehicle{
    public ScooterVehicle(DriveStrategy driveStrategy) {
        super(driveStrategy);
    }
}
