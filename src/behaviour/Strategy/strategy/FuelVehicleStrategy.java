package behaviour.Strategy.strategy;

public class FuelVehicleStrategy implements DriveStrategy {

    @Override
    public void drive(){
        System.out.println("Driving with Fuel");
    }
}
