package behaviour.Strategy.strategy;

public class BatteryVehicleStrategy implements DriveStrategy {

    @Override
    public void drive(){
        System.out.println("Driving with Battery");
    }
}
