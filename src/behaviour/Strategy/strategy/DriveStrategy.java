package behaviour.Strategy.strategy;

public interface DriveStrategy {

    void drive();
}
