package behaviour.Strategy;

import behaviour.Strategy.strategy.DriveStrategy;

public class MiniVehicle extends Vehicle{
    public MiniVehicle(DriveStrategy driveStrategy) {
        super(driveStrategy);
    }
}
