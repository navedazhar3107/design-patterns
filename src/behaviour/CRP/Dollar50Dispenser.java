package behaviour.CRP;

public class Dollar50Dispenser implements DispenseChain{

    private final DispenseChain dispenseChain;

    public Dollar50Dispenser(DispenseChain dispenseChain) {
        this.dispenseChain = dispenseChain;
    }

    @Override
    public void handRequest(Currency currency) {

        if(currency.getAmount()<50)
            dispenseChain.handRequest(currency);
        else {
            int num= currency.getAmount()/50;
            int remainder= currency.getAmount()%50;

            System.out.println("Dispending "+num+ " 50$ note");

            if(remainder!=0)
                dispenseChain.handRequest(new Currency(remainder));
        }

    }
}
