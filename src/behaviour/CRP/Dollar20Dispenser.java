package behaviour.CRP;

public class Dollar20Dispenser implements DispenseChain{


    private final DispenseChain dispenseChain;

    public Dollar20Dispenser(DispenseChain dispenseChain) {
        this.dispenseChain = dispenseChain;
    }

    @Override
    public void handRequest(Currency currency) {

        if(currency.getAmount()<20)
            dispenseChain.handRequest(currency);
        else {
            int num= currency.getAmount()/20;
            int remainder= currency.getAmount()%20;

            System.out.println("Dispending "+num+ " 20$ note");

            if(remainder!=0)
                dispenseChain.handRequest(new Currency(remainder));
        }

    }
}
