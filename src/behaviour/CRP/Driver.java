package behaviour.CRP;

public class Driver {

    private  DispenseChain dispenseChain;

    public Driver(DispenseChain dispenseChain) {
        this.dispenseChain = dispenseChain;
        DispenseChain c2=new Dollar20Dispenser(dispenseChain);
        DispenseChain c3=new Dollar10Dispenser(c2);
    }

    public static void main(String[] args) {

        Driver driver=new Driver(new Dollar50Dispenser(new Dollar20Dispenser(new Dollar10Dispenser(null))));

        int amount =180;

        driver.dispenseChain.handRequest(new Currency(amount));



    }
}
