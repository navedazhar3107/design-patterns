package behaviour.CRP;

public class Dollar10Dispenser implements DispenseChain{


    private final DispenseChain dispenseChain;

    public Dollar10Dispenser(DispenseChain dispenseChain) {
        this.dispenseChain = dispenseChain;
    }

    @Override
    public void handRequest(Currency currency) {

        if(currency.getAmount()<10)
            dispenseChain.handRequest(currency);
        else {
            int num= currency.getAmount()/10;
            int remainder= currency.getAmount()%10;

            System.out.println("Dispending "+num+ " 10$ note");

            if(remainder!=0)
                dispenseChain.handRequest(new Currency(remainder));
        }

    }
}
