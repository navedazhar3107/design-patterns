package behaviour.CRP;

public interface DispenseChain {

//    void setNextChain(DispenseChain dispenseChain);

    void handRequest(Currency currency);
}
