package structural.proxy;

public class Driver {


    public static void main(String[] args) {
        EmployeeOps employeeOps=new EmployeeProxy(new EmployeeOpsImpl());
        employeeOps.create("ADMIN",new Employee(1,"Naved"));
        employeeOps.create("USER",new Employee(2,"Naved"));
        employeeOps.delete("USER",1);
        employeeOps.get("USER",1);



    }
}
