package structural.proxy;

public class Employee {

    private final Integer id;
    private final String name;

    public Employee(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
}
