package structural.proxy;

public class EmployeeOpsImpl implements EmployeeOps{
    @Override
    public void create(String client, Employee employee) {
        System.out.println("Created the employee data :"+employee);
    }

    @Override
    public void delete(String client, Integer id) {
        System.out.println("Deleted the employee data:"+id);
    }

    @Override
    public Employee get(String client, Integer id) {
        System.out.println("Fetched the employee data:"+id);
        return new Employee(id,"Naved");
    }
}
