package structural.proxy;

public interface EmployeeOps {

    void create(String client,Employee employeeOps);
    void delete(String client,Integer id);
    Employee get(String client,Integer id);
}
