package structural.proxy;

import java.util.Objects;

public class EmployeeProxy implements EmployeeOps{

    private final EmployeeOps employeeOps;

    public EmployeeProxy(EmployeeOps employeeOps) {
        this.employeeOps = employeeOps;
    }

    @Override
    public void create(String client, Employee employee) {
        if(Objects.equals(client,"ADMIN"))
            employeeOps.create(client,employee);
        else
            System.out.println("ACCESS DENIED for user: "+client);
    }

    @Override
    public void delete(String client, Integer id) {
        if(Objects.equals(client,"ADMIN"))
            employeeOps.delete(client,id);
        else
            System.out.println("ACCESS DENIED for user: "+client);
    }

    @Override
    public Employee get(String client, Integer id) {
        if(Objects.equals(client,"ADMIN") || Objects.equals(client,"USER"))
            return employeeOps.get(client,id);

        System.out.println("ACCESS DENIED for user: "+client);
        return new Employee(null,"");
    }
}
